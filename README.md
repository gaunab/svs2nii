Nifti Converter
=====================

This Script converts svs images to be used with 3D-Imaging and Segmentation Software like itksnap

![itksnap_hist.jpg](https://bitbucket.org/repo/KL9qXr/images/4261059140-itksnap_hist.jpg)

Requirements
-------------

* [vips](http://www.vips.ecs.soton.ac.uk/): this tool can handle even huge
  images. Its part of the `libvips-tools` package.
* [convert](http://www.imagemagick.org/script/index.php) is Part of
  `imagemagick` and is most likely already installed.
* [c3d](http://www.itksnap.org/pmwiki/pmwiki.php?n=Convert3D.Documentation)
   is part of the itksnap project. It can be downloaded from their project
  website.

Description
-------------

This tool was developed to open svs-Images with ITK-Snap. The purpose of this
was to segment histological Images with ITK-Snap.  It seems that itksnap is not
able to display images which are larger than 8000x8000px. So this script is
scaling the Image down to max 8000px. Also it seems that c3d has a problem with
the tiff-Images produced by vips. For this reason convert from imagemagick is
used to repair the tiff.


Usage
--------

    svs2nii.sh [PATH_TO_SVS_IMAGE]


License
----------
Copyright (c) 2015, Paul Kuntke
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.