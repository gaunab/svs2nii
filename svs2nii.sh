#!/usr/bin/env sh
#
# Use this script to convert Big Images (Histological Slices)
# to VTK Images for use in ITK-Snap
#
# USAGE: svs2nii.sh [SVS-FILE]
# This saves the File as nii.gz in the working directory


input=$1
filename=`basename -s .svs "${input}"`

echo "Found filename ${filename}"
temp=/tmp/${filename}.tiff
temp2=/tmp/${filename}2.tiff

echo "Resizing Image by Rightshift"
vips --vips-progress im_rightshift_size "${input}" ${temp} 2 2 0
echo "Change Colourspace to GRAY16"
vips colourspace "${temp}" "${temp2}" grey16
rm "${temp}"
convert "${temp2}" -resize 8000x8000 "${temp}" 			# Use convert from imagemagick to repair the tiff and resize to max 8000pxt pu:G
echo "Convert to NIFTI" 
c3d "${temp}"  "${filename}".nii.gz
rm ${temp} 
rm ${temp2}
